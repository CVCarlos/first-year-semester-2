import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* This programm takes an input from a named file  and encode the content of
   this document in ASCII code.

   @author Carlos Cerda Veloz
*/
public class Uuencode
{
  public static void main(String[] args)
  {
    FileInputStream input = null;
    try
    {
      //Check for wrong number args before open the files
      if (args.length != 1)
      {
        throw new UuencodeException("Incorrect number of arguments, give the" +
                                    "name " + "of the document to be encoded");
      } //if

      //File reader, read a byte of the document
      input = new FileInputStream(new File(args[0]));

      //Start reading a line of text
      System.out.println("mode 600 " + args[0]);
      int currentByte;
      List<Integer> lineBytes = new ArrayList<Integer>();
      currentByte = input.read();
      while (currentByte != -1)
      {
        lineBytes.clear();
        lineBytes.add(currentByte);
        while((currentByte = input.read()) != -1 && lineBytes.size() < 45)
        {
          lineBytes.add(currentByte);
        }
        int lineBytesLength = lineBytes.size();
        //Fill the empty spaces of the list with a flag value
        while (lineBytes.size() < 45)
          lineBytes.add(0);

        //Added the number of bytes to the new line
        System.out.print(writeByteAsChar(lineBytesLength));
        int index = 0;
        while (index < lineBytesLength)
        {
          //Calculate 4 results bytes from the 3 input bytes
          int byte1 = lineBytes.get(index) >> 2;
          int byte2 = (lineBytes.get(index) & 0x3) << 4
                     | (lineBytes.get(index + 1) >> 4);
          int byte3 = (lineBytes.get(index + 1) & 0xf) << 2
                     | (lineBytes.get(index + 2)  >> 6);
          int byte4 = lineBytes.get(index + 2) & 0x3f;

          //Add those results to the new line
          System.out.print(writeByteAsChar(byte1));
          System.out.print(writeByteAsChar(byte2));
          System.out.print(writeByteAsChar(byte3));
          System.out.print(writeByteAsChar(byte4));

          //Loop to the next 3 bytes
          index += 3;
        }
        System.out.println("");
      }  //while
      System.out.println(writeByteAsChar(0));
      System.out.println("end");
    } //try


    catch (UuencodeException exception)
    {
      //Report UuencodeException to standard System.out
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println("There was an error during the encode"
                         + exception);
    }

    finally
    {
      try
      {
        if (input != null)
          input.close();
      }
      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }
    } //finally
  } //main

  //This method transform a byte code in ASCII code
  private static char writeByteAsChar(int aByte)
  {
    return (char) (aByte == 0 ? 96 : aByte + 32);
  }

} //class Uuencode
