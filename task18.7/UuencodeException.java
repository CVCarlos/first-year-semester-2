/* This class construc a new kind of exception for the Uuencode programm
   @author Carlos Cerda
*/

public class UuencodeException extends Exception
{

  public UuencodeException(String message)
  {
    super(message);
  }

}
