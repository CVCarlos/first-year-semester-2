/*
*  This program takes an input from a command line argument.
*
*  The input is an string and the output is the permutations of this
*  string.
*
*  This programs implements recursivity.
*
*  @author Carlos Cerda Veloz
*
*/
public class Anagrams
{
  private static char[] stringToPermute;
  private static char[] currentPermutation;
  private static boolean[] charUsed;

  public static void main(String[] args)
  {
    try
    {
      if (args.length < 1)
        throw new Exception("Give at least one parameter");
      //Set the arrays to use in the permutation
      stringToPermute = args[0].toCharArray();

      // This arrays contain the permutations
      currentPermutation = new char[stringToPermute.length];

      // An element of this array is true if the char that correspond
      // to that index is being used
      charUsed = new boolean[stringToPermute.length];

      // Call the recursive method with the starting index --> 0
      printPermutations(0);
    }
    catch(Exception exception)
    {
      System.out.println(exception.getMessage());
    }
  }
  // Recursive method used to calculate the anagrams of the string
  public static void printPermutations(int currentIndex)
  {
    // Print the anagrams once the end of the permutation array is reached
    if (currentIndex == currentPermutation.length)
    {
      for (char aChar : currentPermutation)
        System.out.print(aChar);
        System.out.println();
    } // if
    else
    {
      for(int charIndex = 0; charIndex < stringToPermute.length; charIndex++)
      {
        if(!charUsed[charIndex]) // If char is not being used
        {
          charUsed[charIndex] = true; // Set the char has being used
          // Save char in permutation array
          currentPermutation[currentIndex] = stringToPermute[charIndex];
          printPermutations(currentIndex + 1); // Use recursivity for next char
          charUsed[charIndex] = false; // Set char to not being used
        } // if
      } // for
    } // else
  } // printPermutations
} // Anagrams
