/*
  This class accept two objects of a given type an create a pair of them
  @author Carlos Cerda Veloz
*/
public class Pair<FirstType, SecondType>
{
  //The first object
  private final FirstType first;
  //The second object
  private final SecondType second;

  //Create the Pair
  public Pair(FirstType firstObject, SecondType secondObject)
  {
    first = firstObject;
    second = secondObject;
  } //Pair

  //Return first object
  public FirstType getFirst()
  {
    return first;
  } //getFirst

  //Return second object
  public SecondType getSecond()
  {
    return second;
  } //getSecond
}
