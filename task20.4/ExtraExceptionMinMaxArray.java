public class ExtraExceptionMinMaxArray
{
  public static void main(String[] args) throws IllegalArgumentException
  {
    Pair result;

    Double[] nullArray;
    System.out.println();
    System.out.println("===============================================");
    System.out.println("TESTING EXCEPTIONS");
    System.out.println("===============================================");
    System.out.println("Null Array");
    nullArray = new Double[2];
    result = MinMaxArray.getMinMax(nullArray);
  } //main
} //TestMinMaxArray
