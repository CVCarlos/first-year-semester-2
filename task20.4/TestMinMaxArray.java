public class TestMinMaxArray
{
  public static void main(String[] args) throws IllegalArgumentException
  {
    Pair result;

    //Test the results with strings
    String[] stringArray;
    System.out.println("===============================================");
    System.out.println("THE RESULTS USING STRING ARRAYS");
    System.out.println("===============================================");
    System.out.println("Array: hello, a, this, words, monday");
    stringArray = new String[]{"hello", "a", "this", "words", "monday"};
    result = MinMaxArray.getMinMax(stringArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());
    System.out.println("===============================================");
    System.out.println("Array: wednesday, there, pay, dead, own");
    stringArray = new String[]{"wednesday", "there", "pay", "dead", "own"};
    result = MinMaxArray.getMinMax(stringArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());
    System.out.println("===============================================");
    System.out.println("Array: be, there, multiple, dead, own");
    stringArray = new String[]{"be", "there", "multiple", "dead", "own"};
    result = MinMaxArray.getMinMax(stringArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());

    //Test the results with integers
    Integer[] integerArray;
    System.out.println();
    System.out.println("===============================================");
    System.out.println("THE RESULTS USING INTEGER ARRAYS");
    System.out.println("===============================================");
    System.out.println("Array: 3, 4, 9, 12, 13");
    integerArray = new Integer[]{3, 4, 9, 12, 13};
    result = MinMaxArray.getMinMax(integerArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());
    System.out.println("===============================================");
    System.out.println("Array: 11, 12, 23, 27, 23, 12");
    integerArray = new Integer[]{11, 12, 23, 27, 23, 12};
    result = MinMaxArray.getMinMax(integerArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());

    //Test the results with doubles
    Double[] doubleArray;
    System.out.println();
    System.out.println("===============================================");
    System.out.println("THE RESULTS USING DOUBLE ARRAYS");
    System.out.println("===============================================");
    System.out.println("Array: 3.5, 4.8, 9.1, 12.4, 13.5");
    doubleArray = new Double[]{3.5, 4.8, 9.1, 12.4, 13.5};
    result = MinMaxArray.getMinMax(doubleArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());
    System.out.println("===============================================");
    System.out.println("Array: 11.4, 12.3, 23.9, 27.8, 23.2, 12.7");
    doubleArray = new Double[]{11.4, 12.3, 23.9, 27.8, 23.2, 12.7};
    result = MinMaxArray.getMinMax(doubleArray);
    System.out.println("The result is:\n" +
                       "The minimum:" + result.getFirst() + "\n" +
                       "The maximum:" + result.getSecond());

    //Test one exception of the program
    Double[] emptyArray;
    System.out.println();
    System.out.println("===============================================");
    System.out.println("TESTING EXCEPTIONS");
    System.out.println("===============================================");
    System.out.println("Empty Array");
    emptyArray = new Double[]{};
    result = MinMaxArray.getMinMax(doubleArray);
  } //main
} //TestMinMaxArray
