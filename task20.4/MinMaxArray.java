/*
  This class determines the minimum and maximum of an array of comparable
  objects using a generic method
  @author Carlos Cerda Veloz
*/
public class MinMaxArray
{
  //Method that accept array types comparable with itselfs, return a Pair
  //with minimum and maximum items of the array
  public static <ArrayType extends Comparable<ArrayType>> Pair getMinMax(
  ArrayType[] array) throws IllegalArgumentException
  {
    try
    {
      ArrayType minArray = array[0];
      ArrayType maxArray = array[0];
      for(int index = 0; index < array.length; index++)
      {
        // Check the minimum object in the array
        if (minArray.compareTo(array[index]) > 0)
          minArray = array[index];

        // Check the maximum object in the array
        if (maxArray.compareTo(array[index]) < 0)
          maxArray = array[index];
      } //for

      //Return a new pair with the minimum and maximum object of the array
      return new Pair<ArrayType, ArrayType>(minArray, maxArray);
    } //try
    catch (ArrayIndexOutOfBoundsException exception)
    {
      throw new IllegalArgumentException("Array must be non-empty", exception);
    }
    catch (NullPointerException exception)
    {
      throw new IllegalArgumentException("Array must exist", exception);
    }
  }//calculateMinMax
} //MinMaxArray
