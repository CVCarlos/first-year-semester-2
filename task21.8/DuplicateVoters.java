import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/*
*  This programm takes as input a named file which contains data about
*  a voting, the first line of this document cotains the name of the
*  person and the second line the time and location where this person
*  voted
*  The objective of the program is read this document and find duplicate
*  votes in the list.
*  The output is saved in another named file with the duplicate votes
*  information (only the information of the person).
*
*  This version implements the use of a HashMap
*
*  Both, input and output are obtained as command line parameters.
*
*   @author Carlos Cerda Veloz
*/

public class DuplicateVoters
{
  public static void main(String[] args)
  {
    //Input and output variables
    BufferedReader input = null;
    PrintWriter output = null;

    //The HastMap that will contain the information about the votes
    Map<String, String> votesInformation = new HashMap<String, String>();

    try
    {
      //Check for wrong number args before open the files
      if (args.length > 2)
        throw new DuplicateVotersException("Only the name of data file and" +
                                        " output file are required");
      if (args.length < 2)
        throw new DuplicateVotersException("Name of data file and output file"+
                                        " are required");
      //File reader input
      input = new BufferedReader(new FileReader(args[0]));

      //File writer output
      if (new File(args[1]).exists())
         throw new DuplicateVotersException("Output file " + args[1] +
                                        " already exists");

      output = new PrintWriter(new FileWriter(args[1]));

      //Start reading a line of text
      String currentLine;
      String nextLine;
      //Head of the output document
      output.println("These are the duplicate votes found in the document");
      while ((currentLine = input.readLine()) != null)
      {
        nextLine = input.readLine();
        if (votesInformation.get(currentLine) == null)
            votesInformation.put(currentLine, nextLine);
        else
            output.printf("%s%n" +
                          "  Duplicate: %s%n" +
                          "  First ocurrence: %s%n", currentLine, nextLine,
                          votesInformation.get(currentLine));
      } //while
    } //try

    catch (DuplicateVotersException exception)
    {
      //Report DuplicateVotersException to standard output
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }

    finally
    {
      try
      {
        if (input != null)
          input.close();
      }

      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }

      if (output != null)
      {
        output.close();
        if (output.checkError())
          System.err.println("Something bad happend with the output...");
      } //if
    } //finally
  } //main
}
