import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;

/* This programm takes an input from a named file with the lines that need to
   be ordered and puts them ordered in another named file

   @author Carlos Cerda Veloz
*/

public class StreetOrder
{
  public static void main(String[] args)
  {
    List<String> linesRead = new ArrayList<String>();
    BufferedReader input = null;
    PrintWriter output = null;
    try
    {
      //Check for wrong number args before open the files
      if (args.length > 2)
        throw new StreetOrderException("Too many arguments");
      if (args.length < 2)
        throw new StreetOrderException("Give two arguments");

      //Input instance creation
      input = new BufferedReader(new FileReader(args[0]));

      //Output instance creation
      output = new PrintWriter(new FileWriter(args[1]));

      //Start reading each line of text
      String currentLine;
      while ((currentLine = input.readLine()) != null)
      {
        //Add the currentLine to the list of strings
        linesRead.add(currentLine);
      } //while

      //Loop trough the list of strings and print in the document
      //the lines odd numbered
      for(int index = 0; index < linesRead.size(); index = index + 2)
        output.println(linesRead.get(index));

      //Loop trough the list of strings and print in the document
      //the lines even numbered
      //Use this line of code to check if the list is odd or even
      int index = linesRead.size()%2 == 0 ? linesRead.size() - 1:
                                            linesRead.size() - 2;
      while(index > 0)
      {
        output.println(linesRead.get(index));
        index = index - 2;
      }
    } //try

    catch (StreetOrderException exception)
    {
      //Report StreetOrderException to standard output
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }

    finally
    {
      try
      {
        if (input != null)
          input.close();
      }

      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }

      if (output != null)
      {
        output.close();
        if (output.checkError())
          System.err.println("Something bad happend with the output...");
      } //if
    } //finally
    } //main
}
