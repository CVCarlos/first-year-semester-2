public class TestRelativeDates
{
  public static void main(String[] args) throws Exception
  {
    Date referenceDate = new Date(1, 1, 2019);
    int numberOfDates = 0;
    for(int index = 0; index < 731; index++)
    {
      if(referenceDate.getMonth() == 2 && referenceDate.getDay() == 29)
        System.out.println(referenceDate + " a leap date" );
      if(index == 130)
        {
        Date subtractDay = referenceDate.subtractDay();
        System.out.println(subtractDay +" using subtractDay method");
        }
      if(index == 250)
        {
        Date subtractMonth = referenceDate.subtractMonth();
        System.out.println(subtractMonth + " using subtractMonth method" );
        }
      if(index == 360)
        {
        Date subtractYear = referenceDate.subtractYear();
        System.out.println(subtractYear + " using subtractYear method" );
        }
      if(index == 480)
        {
        Date addDay = referenceDate.addDay();
        System.out.println(addDay +" using addDay method");
        }
      if(index == 680)
        {
        Date addMonth = referenceDate.addMonth();
        System.out.println(addMonth +" using addMonth method");
        }
      if(index == 720)
        {
        Date addYear = referenceDate.addYear();
        System.out.println(addYear +" using addYear method");
        }
      referenceDate = referenceDate.addDay();
    }
  }
}
