public class FishTankVolume
{
  public static void main(String[] args)
  {
    //Check for multiple invalid inputs and throw self-explanatory
    //messages
    try
    {
      if(args.length != 3)
        throw new ArrayIndexOutOfBoundsException
          ("You have supplied " + args.length + " arguments");
      for(int index = 0; index < args.length; index++)
      {
        if(!args[index].matches("-?\\d+\\.?\\d*"))
          throw new NumberFormatException
            ("You have to supply numbers for the arguments");
        if(args[index].contains(".") || args[index].contains(","))
          throw new NumberFormatException
            ("You have to supply whole numbers arguments");
        if(Long.parseLong(args[index]) > 2147483647)
            throw new NumberFormatException
              ("You have to supply arguments below 2147483648");
        if(Integer.parseInt(args[index]) < 0)
          throw new NumberFormatException
            ("You have supplied arguments less than cero");
      }
      int width = Integer.parseInt(args[0]);
      int depth = Integer.parseInt(args[1]);
      int height = Integer.parseInt(args[2]);
      int volume = width * depth * height;
      System.out.println("The volume of a tank with dimensions "
                         + "(" + width + "," + depth + "," + height +") "
                         + "is " + volume);
    }

    //Catch all the exceptions that can happen in the programm
    catch(ArrayIndexOutOfBoundsException arrayException)
    {
      System.out.println("Please supply the dimensions, and nothing else");
      System.out.println("Exception message was: `"
                         + arrayException.getMessage());
      System.err.println(arrayException);
    }
    catch(NumberFormatException numberException)
    {
      System.out.println("Please supply valid dimensions");
      System.out.println("Exception message was: `"
                         + numberException.getMessage());
      System.err.println(numberException);
    }
    catch(Exception exception)
    {
      System.out.println("Something unexpected has happened");
      System.out.println("Exception message was: `"
                         + exception.getMessage());
      System.err.println(exception);
    }
  }
}
