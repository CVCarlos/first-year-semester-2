import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeSet;
import java.util.Iterator;

/*
*  This programm takes an input from a named file with the lines that need to
*  be ordered in delivery order.
*
*  Once they are ordered, they are writo in another named file
*
*  This version implements the use of a TreeSet and the Iterator
*
*  @author Carlos Cerda Veloz
*/

public class StreetOrder
{
  public static void main(String[] args)
  {

    //Input and output variables
    BufferedReader input = null;
    PrintWriter output = null;

    //The TreeSet that will contain the information about the addresses
    TreeSet<DeliveryHouseDetails > addressesRead = new TreeSet
                                                   <DeliveryHouseDetails>();
    try
    {
      //Check for wrong number args before open the files
      if (args.length > 2)
        throw new StreetOrderException("Only the name of data file and output"+
                                       "file are required");
      if (args.length < 2)
        throw new StreetOrderException("Name of data file and output file"+
                                        "are required");

      //Input instance creation
      input = new BufferedReader(new FileReader(args[0]));

      //Output instance creation
      output = new PrintWriter(new FileWriter(args[1]));

      //Start reading each line of text
      String currentLine;
      int index = 1;
      while ((currentLine = input.readLine()) != null)
      {
        //Create a new DeliveryHouseDetails instance
        DeliveryHouseDetails aHouse = new DeliveryHouseDetails(index,
                                          currentLine);
        //Add the currentLine to the TreeSet of strings
        addressesRead.add(aHouse);

        index++;
      } //while

      //Iterate trough the TreeSet of strings and print in the document
      //the ordered addresses
      Iterator<DeliveryHouseDetails> addressesIterator =
                                                       addressesRead.iterator();
      while (addressesIterator.hasNext())
      {
        output.println(addressesIterator.next().getPersonName());
      }
    } //try

    catch (StreetOrderException exception)
    {
      //Report StreetOrderException to standard output
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }

    finally
    {
      try
      {
        if (input != null)
          input.close();
      }

      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }

      if (output != null)
      {
        output.close();
        if (output.checkError())
          System.err.println("Something bad happend with the output...");
      } //if
    } //finally
    } //main
}
