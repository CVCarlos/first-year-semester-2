public class LotteryTestB
{
  public static void main(String args[])
  {
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI guiTest = new LotteryGUI("TV Studio Test B", speedController);

    //A worker the fill the machine
    Worker worker1 = new Worker("Test B");

    //Create the machine, add this to the GUI and fill the machine
    Game game1 = new DramaticGame("Test B", 20,"Test B", 20);
    guiTest.addGame(game1);
    worker1.fillMachine(game1);

    //Create the machine and add this to the GUI
    Game game2 = new DramaticGame("Test B", 20,"Test B", 20);
    guiTest.addGame(game2);

    for (int count = 1; count <= game2.getRackSize(); count ++)
    {
      game1.ejectBall();
    } // for

    //Sort the balls and once finish the worker will fill the second game
    worker1.fillMachine(game2);

    for (int count = 1; count <= game2.getRackSize(); count ++)
    {
      game2.ejectBall();
    } // for
  }
}
