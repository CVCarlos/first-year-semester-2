/*
  This class is used to test the generic class MoodyGroup
   @author Carlos Cerda Veloz
*/

public class TestMoodyGroup
{
  public static void main(String[] args)
  {
    // Creating multiple Teenagers and adding this to a new instance of
    // MoodyGroup
    MoodyGroup<Teenager> teenagerGroup = new MoodyGroup<Teenager>();
    teenagerGroup.addMoodyPerson(new Teenager("teenager 1"));
    teenagerGroup.addMoodyPerson(new Teenager("teenager 2"));
    teenagerGroup.addMoodyPerson(new Teenager("teenager 3"));

    System.out.println("These results belong to the teenager group");
    // Test setHappy with false parameter
    for (int index = 0; index < teenagerGroup.getSize(); index++)
      teenagerGroup.setHappy(false);

    // Print the group teenagerGroup
    System.out.println("This result was obtained when setHappy was called"
                       + " with the parameter false");
    System.out.printf("%s%n%n", teenagerGroup);

    // Test setHappy with true parameter
    for (int index = 0; index < teenagerGroup.getSize(); index++)
      teenagerGroup.setHappy(true);

    // Print the group teenagerGroup
    System.out.println("This result was obtained when setHappy was called"
                       + " with the parameter true");
    System.out.printf("%s%n%n", teenagerGroup);

    MoodyGroup<MoodyPerson> workerGroup = new MoodyGroup<MoodyPerson>();
    workerGroup.addMoodyPerson(new Worker("worker 1"));
    workerGroup.addMoodyPerson(teenagerGroup.getMoodyPerson(1));

    System.out.println("These results belong to the worker group");
    // Test setHappy with false parameter
    for (int index = 0; index < workerGroup.getSize(); index++)
      workerGroup.setHappy(true);

    // Print the group workerGroup
    System.out.println("This result was  obtained when setHappy was called"
                       + " with the parameter true");
    System.out.printf("%s%n%n", workerGroup);

    // Test setHappy with true parameter
    for (int index = 0; index < workerGroup.getSize(); index++)
      workerGroup.setHappy(false);

    // Print the group workerGroup
    System.out.println("This result was obtained when setHappy was called"
                       + " with the parameter false");
    System.out.printf("%s%n%n", workerGroup);

    // Print the group teenagerGroup
    System.out.println("This result belongs to the teenager group");
    System.out.println("It was obtained after call setHappy in"
                       + "worker group with the parameter false");
    System.out.printf("%s%n%n", teenagerGroup);
  } // main
} // class
