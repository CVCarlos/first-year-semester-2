import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class MagicWorker extends Worker
{
	public MagicWorker(String name)
	{
		super(name);
	}
	//List for storing the magic balls created
  List<MagicBall> magicBallList = new ArrayList<MagicBall>();

	//This method call the doMagic method for each ball created by the magic
	//worker
	public void doMagic(int spellNumber)
	{
    for(int index = 0; index < magicBallList.size(); index++)
		  magicBallList.get(index).doMagic(spellNumber);
	}

  @Override //Check if this override the method in Worker
	//Override the method makeImage in Worker, change: Return MagicWorkerImage.
  public MagicWorkerImage makeImage()
	{
		return new MagicWorkerImage(this);
	}

  @Override //Check if this override the method in Worker
	//Return the type of Person
	public String getPersonType()
	{
    return "Magic Worker";
	}

  @Override //Check if this override the method in Worker
	//Return the hierarchy of the class
	public String getClassHierarchy()
  {
    return this.getClass().getSimpleName() + ">" + super.getClassHierarchy();
  }

	@Override //Check if this override the method in Worker
	//Create a new magic ball and add this to the list
	public MagicBall makeNewBall(int number, Color colour)
	{
		MagicBall newMagicBall = new MagicBall(number, colour);
		magicBallList.add(newMagicBall);
    return newMagicBall;
	}
}
