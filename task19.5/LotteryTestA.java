public class LotteryTestA
{
  public static void main(String args[])
  {
    //Create a GUI an set each of the Person subclasses
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI guiTest = new LotteryGUI("TV Studio Test A", speedController);

    TVHost tvHost = new TVHost("Test A");
    guiTest.addPerson(tvHost);

    AudienceMember audienceMember1 = new AudienceMember("Test A");
    guiTest.addPerson(audienceMember1);

    Punter punter1 = new Punter("Test A");
    guiTest.addPerson(punter1);

    Psychic psychic = new Psychic("Test A");
    guiTest.addPerson(psychic);

    AudienceMember audienceMember2 = new AudienceMember("Test A");
    guiTest.addPerson(audienceMember2);

    Director director = new Director("Test A");
    guiTest.addPerson(director);

    CleverPunter cleverPunter1 = new CleverPunter("Test A");
    guiTest.addPerson(cleverPunter1);

    Worker worker1 = new TraineeWorker("Test A", 0);
    guiTest.addPerson(worker1);

    CleverPunter cleverPunter2 = new CleverPunter("Test A");
    guiTest.addPerson(cleverPunter2);

    Worker worker2 = new Worker("Test A");
    guiTest.addPerson(worker2);
  }
}
