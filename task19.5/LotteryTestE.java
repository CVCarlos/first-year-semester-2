import java.util.Scanner;
import java.awt.Color;

public class LotteryTestE
{
  private static Scanner inputScanner = new Scanner(System.in);
  public static void main(String args[])
  {
    System.out.print("Choose the extension(1, 2, or 3):");
    int extension = inputScanner.nextInt();
    MagicBall.extension = extension;
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI guiTest = new LotteryGUI("TV Studio Test E", speedController);

    //A worker the fill the machine
    Worker worker1 = new Worker("Test E");
    guiTest.addPerson(worker1);
    Worker worker2 = new MagicWorker("Test E");
    guiTest.addPerson(worker2);

    //Create the machine, add this to the GUI and fill the machine
    Game game1 = new DramaticGame("Test E", 20,"Test E", 8);
    guiTest.addGame(game1);
    //Fill with magic balls
    for(int index = 0; index < 10; index++)
    {
      MagicBall magicBall = new MagicBall(index, Color.white);
      game1.machineAddBall(magicBall);
    }
    worker1.fillMachine(game1);

    //Create the machine and add this to the GUI
    Game game2 = new DramaticGame("Test E", 20,"Test E", 8);
    guiTest.addGame(game2);

    for (int count = 1; count <= game2.getRackSize(); count ++)
    {
      game1.ejectBall();
    }

    //Sort the balls and once finish the worker will fill the second game
    worker2.fillMachine(game2);

    for (int count = 1; count <= game2.getRackSize(); count ++)
    {
      game2.ejectBall();
    } // for
  }
}
