public class LotteryTestD
{
  public static void main(String args[])
  {
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI guiTest = new LotteryGUI("TV Studio Test D", speedController);

    //A worker the fill the machine
    MagicWorker worker1 = new MagicWorker("Test D");
    guiTest.addPerson(worker1);
    MagicWorker worker2 = new MagicWorker("Test D");
    guiTest.addPerson(worker2);

    //Create the machine, add this to the GUI and fill the machine
    Game game1 = new DramaticGame("Test D", 20,"Test D", 8);
    guiTest.addGame(game1);
    worker1.fillMachine(game1);

    //Create the machine and add this to the GUI
    Game game2 = new DramaticGame("Test D", 20,"Test D", 8);
    guiTest.addGame(game2);

    for (int count = 1; count <= game2.getRackSize(); count ++)
    {
      game1.ejectBall();
    } // for

    //Sort the balls and once finish the worker will fill the second game
    worker2.fillMachine(game2);

    for (int count = 1; count <= game2.getRackSize(); count ++)
    {
      game2.ejectBall();
    } // for
  }
}
