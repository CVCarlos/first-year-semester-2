public class DramaticGame extends Game
{
  //Constructor method
  public DramaticGame(String machineName, int machineSize,
                      String rackName, int rackSize)
  {
    super(machineName, machineSize,rackName, rackSize);
  } //DramaticGame

  @Override
  //Overried makeMachine method in Game, other changes were done in order
  //to have a reference to each game created.
  public DramaticMachine makeMachine(String machineName, int machineSize)
  {
    DramaticMachine newDramaticMachine =
    new DramaticMachine(machineName, machineSize);
    newDramaticMachine.setReferenceGame(this);
    return newDramaticMachine;
  }
}
