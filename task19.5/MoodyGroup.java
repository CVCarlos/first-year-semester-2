/*
  This class contains a collection of some subclass of MoodyPerson objects,
   including a method setHappy()
   Note: This is a generic class

   @author Carlos Cerda Veloz
*/
import java.util.ArrayList;
import java.util.List;

public class MoodyGroup<MoodyPersonType extends MoodyPerson>
{
  private List<MoodyPerson> groupList = new ArrayList<MoodyPerson>();

  private int numberMoodyPersons = 0;

  public MoodyGroup() {} // Empty constructor

  // Add a new Moody Person to the group
  public void addMoodyPerson(MoodyPersonType newMoodyPerson)
  {
    groupList.add(newMoodyPerson);
    numberMoodyPersons++;
  } // addMoodyPerson

  public MoodyPerson getMoodyPerson(int index)
  {
    return groupList.get(index);
  }
  // Return Moody Group size
  public int getSize()
  {
    return numberMoodyPersons;
  } // getSize

  // Keep the track of the next Moody Person in be set happy
  private int nextToSetHappy = 0;

  // Set to the next Moody Person happy
  public void setHappy(boolean parameter)
  {
    if (numberMoodyPersons > 0)
    {
      groupList.get(nextToSetHappy).setHappy(parameter);

      nextToSetHappy = (nextToSetHappy + 1) % numberMoodyPersons;
    }
  } // setHappy

  // It is used for testing
  //@Override  
  public String toString()
  {
    String result = "";
    String isHappyResult;
    if (numberMoodyPersons == 0)
      return result;
    for(int index = 0; index < numberMoodyPersons; index++)
    {
      isHappyResult = (groupList.get(index).isHappy() ? "happy" : "unhappy");
      result += String.format("%n%s" + " and I am "
                              + isHappyResult, groupList.get(index));
    }
    return result;
  }
} // MoodyGroup
