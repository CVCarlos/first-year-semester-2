import java.awt.Color;

public class LotteryTestC
{
  public static void main(String args[])
  {
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI guiTest = new LotteryGUI("TV Studio Test C", speedController);

    //Create the machine, add this to the GUI and fill the machine
    Game game = new Game("Test C", 10,"Test C", 10);
    for(int index = 0; index < 10; index++)
    {
      MagicBall magicBall = new MagicBall(10, Color.white);
      game.machineAddBall(magicBall);
    }
    guiTest.addGame(game);

    for (int count = 1; count <= game.getRackSize(); count ++)
    {
      game.ejectBall();
    } // for
  }
}
