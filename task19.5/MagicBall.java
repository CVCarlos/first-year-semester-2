import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class MagicBall extends Ball
{
  //List to save the new Magic Balls created
  private static List<MagicBall> magicBallList = new ArrayList<MagicBall>();

  //Constructor
  public MagicBall (int requiredValue, Color requiredColour)
  {
    //if(BallContainer instanceof DramaticMachine )
    super(requiredValue, requiredColour);
    magicBallList.add(this);
  }
  //Enum for the four states of the ball
  private enum States
  {
  NORMAL, INVISIBLE, FLASHING, COUNTING, SHARED
  }
  private States ballState = States.NORMAL;

  @Override //Check if this override the method in Ball
  //Override the method makeImage in Ball class, change: Return MagicBallImage
  public MagicBallImage makeImage()
  {
    return new MagicBallImage(this);
  } //makeImage

  //Last value variable, start in -1 so in first count this will show 0;
  private int lastValue = -1;
  @Override //heck if this override the method in Ball
  //This method return the value in the ball, if COUNTING state active, return
  //a value from 0-99(constantly changing)
  public int getValue()
  {
    if(ballState == States.SHARED)
    {
      return meanAverage;
    }
    if(ballState == States.COUNTING)
    {
      if(lastValue == 99)
        lastValue = 0;
      else
        lastValue++;
      return lastValue;
    }
    else
      return super.getValue();
  }
  private States trackSpell = States.NORMAL;
  //This method changes the state of the ball when called, if one this will
  //change to the next state. If two this changes to NORMAL state.
  public static int extension;
  public void doMagic(int spellNumber)
  {
    switch (spellNumber)
    {
      case 1:
        switch(ballState)
        {
          case NORMAL: ballState = States.INVISIBLE; break;
          case INVISIBLE: ballState = States.FLASHING; break;
          case FLASHING: ballState = States.COUNTING; break;
          case COUNTING: ballState = States.NORMAL; break;
          default:;
        }
      break;
      case 2: ballState = States.NORMAL; break;
      case 3:
      switch (extension)
      {
        case 1: E1(); break;
        case 2: E2(); break;
        case 3:
        switch(ballState)
        {
          case NORMAL: ballState = States.INVISIBLE; break;
          case INVISIBLE: ballState = States.FLASHING; break;
          case FLASHING: ballState = States.COUNTING; break;
          case COUNTING: ballState = States.SHARED; E3(); break;
          case SHARED: ballState = States.NORMAL; E3(); break;
          default:;
        }
      }
      break;
      default:;
    }
    this.getImage().update();
  }

  //Extension E1
  //All the balls enter in the same state that the one selected
  private DramaticMachine machineIndex;
  private int indexBall;
  public void E1()
  {
    for(int index = 0; index < magicBallList.size(); index++)
    {
      if(magicBallList.get(index) == this)
      {
        indexBall = index;
        machineIndex = magicBallList.get(index).machineReference;
      }
    }
    // Saving the state of the selected ball
    States newState = ballState;
    //Looking through the list of objects
    for(int index = 0; index < magicBallList.size(); index++)
      if(magicBallList.get(index).machineReference != null &&
         magicBallList.get(index).machineReference == machineIndex
         && (index == (indexBall + 1) || index == (indexBall -1)
         || index == indexBall))
       magicBallList.get(index).setState(newState);
  }

  //Extension E2
  //A ball in a machine can switch positions with other ones
  private int numberBallReference;
  private int swapBallIndex;
  public void E2()
  {
    if(machineReference != null) //check if the ball is the machine yet
    {
      for(int index = 0; index < machineReference.getNoOfBalls(); index++)
      {
        if(machineReference.getBall(index) == this)
          numberBallReference = index;
      }
      do //check for another ball
      swapBallIndex = (int) (Math.random() * machineReference.getNoOfBalls());
      while(swapBallIndex == numberBallReference);
      machineReference.swapBalls(numberBallReference, swapBallIndex);
      numberBallReference = swapBallIndex;
    }
  }

  //Extension E3
  private static int meanAverage;
  //The method determines the average of the balls and then update the imagen
  //of all of them
  public void E3()
  {
    for(int index = 0; index < magicBallList.size(); index++)
    {
      if(magicBallList.get(index) == this)
        machineIndex = magicBallList.get(index).machineReference;
    }
    meanAverage = 0;
    int indexBallInMachine = 0;
    //This determines the value average of the balls in shared state
    //that are also in the machine
    for(int index = 0; index < magicBallList.size(); index++)
    {
      if(magicBallList.get(index).machineReference == machineIndex &&
         magicBallList.get(index).ballState == States.SHARED)
      {
        meanAverage += magicBallList.get(index).getRealValue();
        indexBallInMachine += 1;
      }
    }
    if(indexBallInMachine > 0)
      meanAverage /= indexBallInMachine;
    //Update the image of the balls in shared state
    for(int index = 0; index < magicBallList.size(); index++)
    {
      if(magicBallList.get(index).ballState == States.SHARED)
        magicBallList.get(index).getImage().update();
    }
  }

  //Method to get the real value of each image
  public int getRealValue()
  {
    return super.getValue();
  }

  //Mutator method that will change the state of all the balls
  public void setState(States newState)
  {
    ballState = newState;
    doMagic(1);
  }
  //This method is used to keep a reference to the machine
  private DramaticMachine machineReference;
  public void referenceBall(DramaticMachine machine)
  {
    machineReference = machine;
  }

  //Return false if the method require ball to be no visible
  public boolean isVisible()
  {
    if(ballState == States.INVISIBLE)
      return false;
    else
      return true;
  }

  //Return false if the method require ball to be no flashing
  public boolean isFlashing()
  {
    if(ballState == States.COUNTING || ballState == States.FLASHING)
      return true;
    else
      return false;
  }
}
