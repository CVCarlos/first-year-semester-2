public class DramaticMachine extends Machine
{
  //Constructor method
  public DramaticMachine(String name, int size)
  {
    super(name, size);
  } //DramaticMachine

  @Override
  //Returns the name of the type of BallContainer, overried Machine method
  public String getType()
  {
    return "Lottery Dramatic Machine";
  }  //getType

  @Override
  //Overried the ejectBall method in Machine, changes how the machine works,
  //now a ball is selected randomly but before be ejected all the balls starting
  //from the first one will be flashed until reach the selected ball
  public Ball ejectBall()
  {
    if (getNoOfBalls() <= 0)
      return null;
    else
    {
      int ejectedBallIndex = (int) (Math.random() * getNoOfBalls());
      Ball ejectedBall = getBall(ejectedBallIndex);
      if(ejectedBall instanceof MagicBall)
      {
          //Ball ejectedBall = getBall(index);
          ((MagicBall)ejectedBall).referenceBall(null);
      }
      for(int index = 0; index < ejectedBallIndex; index++)
      {
        getBall(index).flash(2, 2);
      }
      swapBalls(ejectedBallIndex, getNoOfBalls() - 1);
      removeBall();

      return ejectedBall;
    }
  }

  @Override //Check if this method override the one in Machine
  public void addBall(Ball ball)
  {
    super.addBall(ball);
    if(ball instanceof MagicBall)
    {
      ((MagicBall)ball).referenceBall(this);
    }
  }

  //This method will save a reference to each game created
  private DramaticGame gameReference;
  public void setReferenceGame(DramaticGame reference)
  {
    gameReference = reference;
  }

  public DramaticGame getReferenceGame()
  {
    return gameReference;
  }

}
