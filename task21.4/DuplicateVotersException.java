
/* This class construc a new kind of exception for the DuplicateVoters programm
   @author Carlos Cerda
*/

public class DuplicateVotersException extends Exception
{
  public DuplicateVotersException(String message)
  {
    super(message);
  }
}
