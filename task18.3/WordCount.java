import java.io.InputStreamReader;
import java.io.IOException;

public class WordCount
{
  public static void main(String[] args)
  {
    InputStreamReader input = new InputStreamReader(System.in);
    int wordCount = 0;
    try
    {
      boolean previousCharacterIsWhite = false;
      boolean firstWord;
      boolean startOfWord = false;
      boolean endOfWord = false;
      int currentCharacter;
      while ((currentCharacter = input.read()) != -1)
      {
        //Get the character that corresponds to the this value
        char character = ((char) currentCharacter);

        //Check if it is the first word
        if (wordCount == 0)
          firstWord = true;
        else
          firstWord = false;

        //Check if the character is white space, if it is check if this
        //represent the end of a word
        if (Character.isWhitespace(character))
        {
          previousCharacterIsWhite = true;
          if (startOfWord)
          {
            startOfWord = false;
            endOfWord = true;
          }
        }

        //If the character meet the requeriments to be start of a word
        //then mark the start of a new word
        if (!Character.isWhitespace(character) && (previousCharacterIsWhite
                                                   || firstWord))
          startOfWord = true;

        //If the end of a word is marked then add one to the word count
        if (!startOfWord && endOfWord)
        {
          wordCount += 1;
          endOfWord = false;
        }
      } //while
    } //try
    catch(IOException exception)
    {
      System.err.println(exception);
    } //catch
    finally
    {
      try
      {
        input.close();
      } //try
      catch(IOException exception)
      {
        System.err.println("Could not close input " + exception);
      } //catch
    } //finally

    //Report results
    System.out.println("The number of words was " + wordCount);
  }
}
