import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;

/* This programm takes an input from standard input or a named file
   and puts its output on standard output or in another named file

   @author Carlos Cerda Veloz
*/

public class DeleteField
{
  public static void main(String[] args)
  {
    BufferedReader input = null;
    PrintWriter output = null;
    int fieldToDelete;
    try
    {
      //Check for wrong number args before open the files
      if (args.length > 3)
        throw new DeleteFieldException("Too many arguments");
      if (args.length < 1)
        throw new DeleteFieldException("Give at least the field to delete");

      //Input instance creation
      if (args.length < 2 || args[0].equals("-"))
      {
        //Standard input
        input = new BufferedReader(new InputStreamReader(System.in));
      } //if
      else
        //File reader input
        input = new BufferedReader(new FileReader(args[0]));

      //Output instance creation
      if (args.length < 3 || args[1].equals("-"))
        output = new PrintWriter(System.out, true); //Standard output
      else
      {
        if (new File(args[1]).exists()) //File writer output
          throw new DeleteFieldException("Output file "
                                         + args[1] + "already exists");

          output = new PrintWriter(new FileWriter(args[1]));
      } //else
      fieldToDelete = Integer.parseInt(args[(args.length - 1)]);
      if (fieldToDelete < 0)
        throw new DeleteFieldException("Field to delete must be" +
                                        " greater than 0");
      //Start reading a line of text
      String currentLine;
      while ((currentLine = input.readLine()) != null)
      {
        //Divide the line into field using tab as a delimiter
        String[] fields = currentLine.split("(\\t+|\\s+)+");
        String editedLine = "";
        if(fields.length < fieldToDelete)
          editedLine = currentLine;
        else
        {
          //Build a new line in parts
          //Add the fields before the one to be deleted
          for (int index = 0; index < fieldToDelete - 1; index++)
            if (editedLine.equals(""))
              editedLine = fields[index];
            else
              editedLine += "  " + fields[index];

          //Add the fields after the one to be deleted
          for (int index = fieldToDelete; index < fields.length; index++)
          {
            if (editedLine.equals(""))
              editedLine = fields[index];
            else
              editedLine += "  " + fields[index];
          }
        } //else
        output.println(editedLine);
      } //while
    } //try

    catch (NumberFormatException exception)
    {
      System.out.println("Field to delete must be integer");
      System.err.println(exception.getMessage());
    }

    catch (DeleteFieldException exception)
    {
      //Report DeleteFieldException to standard output
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }

    finally
    {
      try
      {
        if (input != null)
          input.close();
      }

      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }

      if (output != null)
      {
        output.close();
        if (output.checkError())
          System.err.println("Something bad happend with the output...");
      } //if
    } //finally
  } //main
} //class DeleteField
