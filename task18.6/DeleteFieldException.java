/* This class construc a new kind of exception for the DeleteField programm
   @author Carlos Cerda
*/

public class DeleteFieldException extends Exception
{

  public DeleteFieldException(String message)
  {
    super(message);
  }

}
