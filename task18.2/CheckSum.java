import java.io.IOException;

public class CheckSum
{
  public static void main(String[] args)
  {
    int checkSum = 0;
    int currentByte;
    try
    {
      while ((currentByte = System.in.read()) != -1)
      {
        if ((checkSum % 2) == 0)
          checkSum = checkSum>>>1;
        else
        {
          checkSum = checkSum>>>1;
          checkSum += 0x8000;
        }
        checkSum += currentByte;
        if (checkSum >= 65536)
          checkSum -= 65536;
      } //while
    } //try
    catch (IOException exception)
    {
      System.err.println("There was an error during the check sum"
                         + exception.getMessage());
    } //catch
    finally
    {
      try
      {
        System.in.close();
      } //try
      catch (IOException exception)
      {
        System.err.println("There was an error at close the program"
                           + exception.getMessage());
      } //catch
    } //finally

    //Print the check sum result
    System.out.println("The check is " + checkSum);
  } //main
} // class CheckSum
