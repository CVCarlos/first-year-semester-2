// Program to measure how much the mean of the integer command line arguments
// differs from the average of their minimum and maximum
// (Warning: this program does not catch RuntimeExceptions)
// Note: This program uses autoboxing 
public class MeanMinMaxMinusMean
{
  public static void main(String[] args) throws RuntimeException
  {
    try
    {
      int[] array = new int[args.length];
      for(int index = 0; index < args.length; index++)
      {
        //Check for multiple invalid inputs and throw self-explanatory
        //messages
        if (!args[index].matches("-?\\d+"))
          throw new NumberFormatException
            ("You have to supply whole numbers for the arguments");

        //If argument is correct, add this to the array
        array[index] = Integer.parseInt(args[index]);
      } // for

      // Create an object triple using the generic class
      Triple<Integer, Integer, Double> stats= IntArrayStats.getStats(array);
      int max = stats.getFirst();
      int min = stats.getSecond();
      double mean = stats.getThird();
      System.out.println((min + max) / 2.0 - mean);
    } // try
    catch(NumberFormatException exception)
    {
      System.err.println(exception.getMessage());
    }
  } // main
} // class MeanMinMaxMinusMean
