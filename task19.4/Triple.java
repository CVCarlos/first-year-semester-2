/*
  This generic class with three diferent types arguments

  @author Carlos Cerda Veloz
*/
public class Triple<FirstType, SecondType, ThirdType>
{
  // The first object
  private final FirstType first;

  // The second object
  private final SecondType second;

  // The third object
  private final ThirdType third;

  public Triple(FirstType firstObject, SecondType secondObject,
                 ThirdType thirdObject)
  {
    first = firstObject;
    second = secondObject;
    third = thirdObject;
  } // Triple

  public FirstType getFirst()
  {
    return first;
  } // getFirst

  public SecondType getSecond()
  {
    return second;
  } // getSecond

  public ThirdType getThird()
  {
    return third;
  } // getThird

} // class Triple
