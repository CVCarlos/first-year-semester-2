/*
  This class will take an array of ints and returns a Triple containin the
  maximum integer in the array, the minimum and also the mean of all the values
  Note: This program uses autoboxing 
  @author Carlos Cerda Veloz
*/
public class IntArrayStats
{
  public static Triple<Integer, Integer, Double> getStats(int[] array)
                                          throws IllegalArgumentException
  {
    if(array == null || array.length == 0)
     throw new IllegalArgumentException("Array must exist and must not be"
                                         + " empty");
    int maximumInteger;
    int minimumInteger;
    int sumIntegers = 0;
    double meanIntegers;
    maximumInteger = array[0];
    minimumInteger = array[0];
    for(int x = 0; x < array.length; x++)
    {
      // Check the maximum integer
      if (array[x] > maximumInteger)
      maximumInteger = array[x];

      // Check the minimum integer
      if (array[x] < minimumInteger)
      minimumInteger = array[x];

      // Current sum of integers
      sumIntegers += array[x];
    } // for

    // The mean of all the values in the array
    meanIntegers = sumIntegers / (double) array.length;
    return new Triple<Integer, Integer, Double>(maximumInteger, minimumInteger,
                                                 meanIntegers);

  } // main
} //IntArrayStats
