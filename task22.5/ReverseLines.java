import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;

/*
*  This programm takes as input a named file which contains a set of lines.
*
*  The objective of the program is use a reverse method to output the lines
*  in a way that the last line will be the first line.
*
*  The output is saved in another named file.
*
*  This version implements recursivity
*
*  Both, input and output are obtained as command line parameters.
*
*   @author Carlos Cerda Veloz
*/

public class ReverseLines
{
  public static void main(String[] args)
  {
    //Input and output variables
    BufferedReader input = null;
    PrintWriter output = null;

    try
    {
      //Check for wrong number args before open the files
      if (args.length > 2)
        throw new ReverseLinesException("Only the name of data file and" +
                                        " output file are required");
      if (args.length < 2)
        throw new ReverseLinesException("Name of data file and output file"+
                                        " are required");
      //File reader input
      input = new BufferedReader(new FileReader(args[0]));

      //File writer output
      output = new PrintWriter(new FileWriter(args[1]));

      reverseLines(input, output);
    } //try
    catch (ReverseLinesException exception)
    {
      //Report ReverseLinesException to standard output
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }
    finally
    {
      try
      {
        if (input != null)
          input.close();
      }

      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }

      if (output != null)
      {
        output.close();
        if (output.checkError())
          System.err.println("Something bad happend with the output...");
      } //if
    } //finally
  }

  //Recursive method used to reverse the input
  private static void reverseLines(BufferedReader aReader, PrintWriter aPrinter)
  {
    try
    {
      //Start reading a line of text
      String headLine = aReader.readLine();
      if (headLine != null)
      {
        reverseLines(aReader, aPrinter);
        aPrinter.println(headLine);
      } //if
    } //try
    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }
  } //reverseLines
}
