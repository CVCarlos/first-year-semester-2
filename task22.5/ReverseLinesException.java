
/* This class construc a new kind of exception for the ReverseLines programm
   @author Carlos Cerda
*/

public class ReverseLinesException extends Exception
{
  public ReverseLinesException(String message)
  {
    super(message);
  }
}
