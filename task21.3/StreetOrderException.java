/* This class construc a new kind of exception for the StreetOrder programm
   @author Carlos Cerda
*/

public class StreetOrderException extends Exception
{

  public StreetOrderException(String message)
  {
    super(message);
  }

}
