import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;

/* This programm takes an input from a named file with the lines that need to
   be ordered and puts them ordered in another named file.
   This program use the compareTo() method

   @author Carlos Cerda Veloz
*/

public class StreetOrder
{
  public static void main(String[] args)
  {
    List<DeliveryHouseDetails> listOfHouses =
                                         new ArrayList<DeliveryHouseDetails>();
    BufferedReader input = null;
    PrintWriter output = null;
    try
    {
      //Check for wrong number args before open the files
      if (args.length > 2)
        throw new StreetOrderException("Too many arguments");
      if (args.length < 2)
        throw new StreetOrderException("Give two arguments");

      //Input instance creation
      input = new BufferedReader(new FileReader(args[0]));

      //Output instance creation
      output = new PrintWriter(new FileWriter(args[1]));

      //Start reading each line of text
      String currentLine;
      int indexHouse = 0;
      while ((currentLine = input.readLine()) != null)
      {
        indexHouse++;
        //Construct a DeliveryHouseDetails with the information of the house
        //Add the currentLine to the list of strings
        listOfHouses.add(new DeliveryHouseDetails(indexHouse, currentLine));
      } //while

      //Order the list in the required Order
      Collections.sort(listOfHouses);

      //Output the result
      for (int index = 0; index < listOfHouses.size(); index++)
        output.println(listOfHouses.get(index).getPersonName());
    } //try

    catch (StreetOrderException exception)
    {
      //Report StreetOrderException to standard output
      System.out.println(exception.getMessage());
    }

    catch (IOException exception)
    {
      //Other exceptions go to standard error
      System.err.println(exception);
    }

    finally
    {
      try
      {
        if (input != null)
          input.close();
      }

      catch (IOException exception)
      {
        System.err.println("Could not close input" + exception);
      }

      if (output != null)
      {
        output.close();
        if (output.checkError())
          System.err.println("Something bad happend with the output...");
      } //if
    } //finally
    } //main
}
