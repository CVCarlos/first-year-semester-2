/*
   This class construct an object using two instaces variables
   The first instance variable represent the number of the house and the second
   one represents the name of person.
   This class implements Comparable interface

   @author Carlos Cerda Veloz
*/

public class DeliveryHouseDetails implements Comparable<DeliveryHouseDetails>
{
  //Two instances variables
  private final int houseNumber;
  private final String personName;

  //Constructor method
  public DeliveryHouseDetails(int aHouseNumber, String aPersonName)
  {
    houseNumber = aHouseNumber;
    personName = aPersonName;
  }

  //This method will be used to sort a list of DeliveryHouseDetails
  //This method will override the method in the Comparable interface
  @Override
  public int compareTo(DeliveryHouseDetails anotherHouse)
  {
    if(houseNumber % 2 == 1 && anotherHouse.houseNumber % 2 == 1)
      return (houseNumber - anotherHouse.houseNumber);
    else if(houseNumber % 2 == 0 && anotherHouse.houseNumber % 2 == 0)
      return (anotherHouse.houseNumber - houseNumber);
    else if(houseNumber % 2 == 1)
      return -1;
    else
      return 1;
  } //compareTo

  //This method is an equivalence test, needs to be consistent with
  //compareTo
  @Override
  public boolean equals(Object anotherObject)
  {
    if(anotherObject instanceof DeliveryHouseDetails)
      return houseNumber == ((DeliveryHouseDetails)anotherObject).houseNumber;
    else
      return super.equals(anotherObject);
  } //equals

  //Accessor method for house number
  public int getHouseNumber()
  {
    return houseNumber;
  }

  //Accessor method for person name
  public String getPersonName()
  {
    return personName;
  }

}
